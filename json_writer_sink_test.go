package logr

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"testing"
	"time"
	"unsafe"

	"github.com/stretchr/testify/require"
)

type testJSONEntry struct {
	Timestamp string `json:"timestamp"`
	Event     string `json:"event"`
	KV        KV     `json:"kv"`
}

func TestJSONWriterSink(t *testing.T) {
	var buf bytes.Buffer
	sink := NewJSONWriterSink(&buf)

	kv := KV{
		"string":              "string",
		"string_with_spaces":  "string with spaces",
		"string_with_symbols": "stringwithsymbols$",
		"error":               errors.New("error"),
		"bool":                true,
		"int":                 int(1),
		"int8":                int8(1),
		"int16":               int16(1),
		"int32":               int32(1),
		"int64":               int64(1),
		"uint8":               uint8(1),
		"uint16":              uint16(1),
		"uint32":              uint32(1),
		"uint64":              uint64(1),
		"float32":             float32(1),
		"float64":             float64(1),
		"complex64":           complex64(1),
		"complex128":          complex128(1),
		"untyped_nil":         nil,
		"chan":                make(chan bool),
		"chan_nil":            testNilChan,
		"func":                testFunc,
		"func_nil":            testNilFunc,
		"interface_nil":       (io.Writer)(nil),
		"struct":              testStruct{Error: nil, Duration: time.Second},
		"map": map[int]string{
			1: "one",
			2: "two",
		},
		"map_structs": map[string]testStruct{
			"hello": testStruct{Error: nil, Duration: time.Second},
			"world": testStruct{Error: errors.New("testing error"), Duration: time.Minute},
		},
		"slice":             []int{1, 2, 3},
		"array":             [3]int{1, 2, 3},
		"slice_structs":     []testStruct{testStruct{}, testStruct{}},
		"unsafeptr":         unsafe.Pointer(new(int)),
		"uintptr":           testUintptr,
		"pointer":           testIntptr,
		"typed_nil_pointer": (*int)(nil),
	}

	sink.Event("event", kv)

	expectedKV := KV{
		"string":              "string",
		"string_with_spaces":  "string with spaces",
		"string_with_symbols": "stringwithsymbols$",
		"error":               errors.New("error"),
		"bool":                true,
		"int":                 float64(1),
		"int8":                float64(1),
		"int16":               float64(1),
		"int32":               float64(1),
		"int64":               float64(1),
		"uint8":               float64(1),
		"uint16":              float64(1),
		"uint32":              float64(1),
		"uint64":              float64(1),
		"float32":             float64(1),
		"float64":             float64(1),
		"complex64":           complex64(1),
		"complex128":          complex128(1),
		"untyped_nil":         nil,
		"chan":                make(chan bool),
		"chan_nil":            testNilChan,
		"func":                testFunc,
		"func_nil":            testNilFunc,
		"interface_nil":       (io.Writer)(nil),
		"struct":              testStruct{Error: nil, Duration: time.Second},
		"map": map[int]string{
			1: "one",
			2: "two",
		},
		"map_structs": map[string]testStruct{
			"hello": testStruct{Error: nil, Duration: time.Second},
			"world": testStruct{Error: errors.New("testing error"), Duration: time.Minute},
		},
		"slice":             []float64{1, 2, 3},
		"array":             [3]float64{1, 2, 3},
		"slice_structs":     []testStruct{testStruct{}, testStruct{}},
		"unsafeptr":         unsafe.Pointer(new(int)),
		"uintptr":           testUintptr,
		"pointer":           testFloatptr,
		"typed_nil_pointer": (*int)(nil),
	}
	checkJSONEntry(t, &buf, testJSONEntry{
		Event: "event",
		KV:    expectedKV,
	})
}

func checkJSONEntry(t *testing.T, buf *bytes.Buffer, entry testJSONEntry) {
	var rcvEntry testJSONEntry
	err := json.NewDecoder(buf).Decode(&rcvEntry)
	require.Nil(t, err, "entry should be valid json")

	entry.Timestamp = rcvEntry.Timestamp
	entry.KV = entry.KV.humanized()
	require.Equal(t, entry, rcvEntry, "entries should match")

	timestamp, err := time.Parse(time.RFC3339Nano, rcvEntry.Timestamp)
	require.Nil(t, err, "timestamp should be RFC3339Nano")
	require.WithinDuration(t, time.Now(), timestamp, time.Millisecond, "timestamp should be time.Now")

	buf.Reset()
}
