package logr

import (
	"context"
	"time"

	"github.com/zemirco/uid"
)

// check that Job implements the Receiver interface
var _ Receiver = (*Job)(nil)

var DiscardReceiver = NewJob(Discard, "discard", nil)

type taskContextKey string

const taskContextIDKey taskContextKey = "__logr_task_context_id"

type Job struct {
	StartedAt time.Time
	KVSink
}

func NewJob(sink Sink, name string, kv KV) *Job {
	if sink == nil {
		sink = Discard
	}

	job := &Job{
		StartedAt: time.Now(),
		KVSink: KVSink{
			KV:   MergedKV(kv, KV{"job": name}),
			Sink: sink,
		},
	}

	return job
}

func (j *Job) Event(event string, kv KV) {
	j.KVSink.Event(event, kv)
}

func (j *Job) Error(event string, err error, kv KV) {
	j.KVSink.Event(event, MergedKV(kv, KV{
		"error": err,
	}))
}

func (j *Job) Timing(event string, timing time.Duration, kv KV) {
	j.KVSink.Event(event, MergedKV(kv, KV{
		"timing": timing,
	}))
}

func (j *Job) Gauge(event string, gauge float64, kv KV) {
	j.KVSink.Event(event, MergedKV(kv, KV{
		"gauge": gauge,
	}))
}

func (j *Job) Complete(status CompletionStatus, kv KV) {
	j.KVSink.Event("job_end", MergedKV(kv, KV{
		"status": string(status),
		"timing": time.Since(j.StartedAt),
	}))
}

func (j *Job) DeferDone(err *error, kv KV) {
	j.Done(*err, kv)
}

func (j *Job) Done(err error, kv KV) {
	if err != nil {
		j.Error("error", err, kv)
		j.Complete(Failed, kv)
		return
	}

	j.Complete(Success, kv)
}

func Task(ctx context.Context, name string, eventKV KV) (context.Context, *Job) {
	// Always create a new ID for a task
	jobKV := KV{"task.id": uid.New(10)}

	sink := SinkFromContext(ctx)
	// If this is the very first task, we add context.id, which is available (and remains the same) in all children contexts
	if ctx.Value(taskContextIDKey) == nil {
		ctx = context.WithValue(ctx, taskContextIDKey, true)

		// Create new KVSink, wrapping the current sink, which will be available in all descendants
		sink = KVSink{Sink: sink, KV: KV{"context.id": uid.New(10)}}
		// Add sink to context
		ctx = SinkContext(ctx, sink)
	}

	// If is a subtask, prepend parent task's name to current task name
	if parentJob, ok := FromContext(ctx).(*Job); ok {
		if parentID, ok := parentJob.KV["task.id"]; ok {
			jobKV["task.parent"] = parentID.(string)
		}
	}

	// Create job that will be used for logging by descendants
	job := NewJob(sink, name, jobKV)
	// Add job to context
	ctx = ReceiverContext(ctx, job)

	// Passed in eventKV is only used for the start event
	job.Event("job_start", eventKV)

	return ctx, job
}
