package logr

import (
	"errors"
	"io"
	"testing"
	"time"
	"unsafe"

	"github.com/stretchr/testify/require"
)

var (
	testNilChan  chan bool
	testNilFunc  func()   = nil
	testIntval            = 1
	testFloatval          = float64(1)
	testIntptr   *int     = &testIntval
	testUintptr  uintptr  = uintptr(unsafe.Pointer(&testIntval))
	testFloatptr *float64 = &testFloatval
)

type testStruct struct {
	Error    error
	Duration time.Duration
}

func testFunc() {}

func TestHumanize(t *testing.T) {
	testMapCycle := make(map[string]interface{})
	testMapCycle["testMapCycle"] = testMapCycle
	type testStructCycle struct {
		Next *testStructCycle
	}
	tscCycle1 := &testStructCycle{}
	tscCycle2 := &testStructCycle{Next: tscCycle1}
	tscCycle1.Next = tscCycle2

	type testStructNotCycle struct {
		NextA *testStructCycle
		NextB *testStructCycle
	}
	tsnc := &testStructNotCycle{}
	tsncNext := &testStructCycle{}
	tsnc.NextA = tsncNext
	tsnc.NextB = tsncNext

	testSliceCycle := make([]interface{}, 1)
	testSliceCycle[0] = testSliceCycle

	testCases := []struct {
		desc      string
		data      interface{}
		humanized interface{}
	}{
		{desc: "string", data: "string", humanized: "string"},
		{desc: "error", data: errors.New("error"), humanized: errors.New("error").Error()},
		{desc: "stringer", data: time.Second, humanized: time.Second.String()},
		{desc: "bool", data: true, humanized: true},
		{desc: "int", data: int(1), humanized: int(1)},
		{desc: "int8", data: int8(1), humanized: int8(1)},
		{desc: "int16", data: int16(1), humanized: int16(1)},
		{desc: "int32", data: int32(1), humanized: int32(1)},
		{desc: "int64", data: int64(1), humanized: int64(1)},
		{desc: "uint8", data: uint8(1), humanized: uint8(1)},
		{desc: "uint16", data: uint16(1), humanized: uint16(1)},
		{desc: "uint32", data: uint32(1), humanized: uint32(1)},
		{desc: "uint64", data: uint64(1), humanized: uint64(1)},
		{desc: "float32", data: float32(1), humanized: float32(1)},
		{desc: "float64", data: float64(1), humanized: float64(1)},
		{desc: "complex64", data: complex64(1), humanized: "(1+0i)"},
		{desc: "complex128", data: complex128(1), humanized: "(1+0i)"},
		{desc: "untyped_nil", data: nil, humanized: "<nil>"},
		{desc: "chan", data: make(chan bool), humanized: "<chan>"},
		{desc: "chan_nil", data: testNilChan, humanized: "<nil>"},
		{desc: "func", data: testFunc, humanized: "<func>"},
		{desc: "func_nil", data: testNilFunc, humanized: "<nil>"},
		{desc: "interface_nil", data: (io.Writer)(nil), humanized: "<nil>"},
		{desc: "struct",
			data:      testStruct{Error: nil, Duration: time.Second},
			humanized: map[string]interface{}{"Error": "<nil>", "Duration": "1s"},
		},
		{desc: "map",
			data:      map[int]string{1: "one", 2: "two"},
			humanized: map[string]interface{}{"1": "one", "2": "two"},
		},
		{desc: "map_structs",
			data: map[string]testStruct{
				"hello": testStruct{Error: nil, Duration: time.Second},
				"world": testStruct{Error: errors.New("testing error"), Duration: time.Minute},
			},
			humanized: map[string]interface{}{
				"hello": map[string]interface{}{
					"Error": "<nil>", "Duration": "1s",
				},
				"world": map[string]interface{}{
					"Error": "testing error", "Duration": "1m0s",
				},
			},
		},
		{desc: "slice", data: []int{1, 2, 3}, humanized: []interface{}{1, 2, 3}},
		{desc: "array", data: [3]int{1, 2, 3}, humanized: []interface{}{1, 2, 3}},
		{desc: "slice_structs",
			data: []testStruct{testStruct{}, testStruct{}},
			humanized: []interface{}{
				map[string]interface{}{"Error": "<nil>", "Duration": "0s"},
				map[string]interface{}{"Error": "<nil>", "Duration": "0s"},
			},
		},
		{desc: "unsafeptr", data: unsafe.Pointer(new(int)), humanized: "<unsafe ptr>"},
		{desc: "uintptr", data: testUintptr, humanized: "<uintptr>"},
		{desc: "pointer", data: testIntptr, humanized: *testIntptr},
		{desc: "typed_nil_pointer", data: (*int)(nil), humanized: "<nil>"},
		{desc: "cycle_map", data: testMapCycle, humanized: map[string]interface{}{"testMapCycle": "<cycle>"}},
		{desc: "cycle_struct", data: tscCycle1, humanized: map[string]interface{}{
			"Next": map[string]interface{}{"Next": "<cycle>"},
		}},
		{desc: "nocycle_struct", data: tsnc, humanized: map[string]interface{}{
			"NextA": map[string]interface{}{"Next": "<nil>"},
			"NextB": map[string]interface{}{"Next": "<nil>"},
		}},
		{desc: "cycle_slice", data: testSliceCycle, humanized: []interface{}{"<cycle>"}},
	}
	for _, tc := range testCases {
		t.Run(tc.desc, func(t *testing.T) {
			var vis visited
			humanized := humanize(tc.data, vis)
			require.Equal(t, tc.humanized, humanized)
		})
	}
}
