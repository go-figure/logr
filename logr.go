package logr

import "time"

type CompletionStatus string

var (
	Success = CompletionStatus("success")
	Failed  = CompletionStatus("failed")
	Invalid = CompletionStatus("invalid")
	Junk    = CompletionStatus("junk")
)

type KV map[string]interface{}

type Receiver interface {
	Event(event string, kv KV)
	Error(event string, err error, kv KV)
	Timing(event string, timing time.Duration, kv KV)
	Gauge(event string, gauge float64, kv KV)
}

type Sink interface {
	Event(event string, kv KV)
}

func (kv KV) humanized() KV {
	var vis visited
	return humanize(kv, vis).(map[string]interface{})
}
