package logr

import (
	"bytes"
	"errors"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

type testWriterEntry struct {
	Timestamp string
	Event     string
	KV        KV
}

func TestWriterSink(t *testing.T) {
	var buf bytes.Buffer
	sink := NewWriterSink(&buf)

	kv := KV{
		"key1": 1,
		"key2": "val",
		"key3": true,
	}
	err := errors.New("testing error")
	timing := 42 * time.Second
	gauge := 0.42

	sink.Event("event", kv)
	checkEntry(t, &buf, testWriterEntry{
		Event: "event",
		KV:    kv,
	})

	sink.Event("event", MergedKV(kv, KV{"error": err}))
	checkEntry(t, &buf, testWriterEntry{
		Event: "event",
		KV:    MergedKV(kv, KV{"error": err}),
	})

	sink.Event("event", MergedKV(kv, KV{"timing": timing}))
	checkEntry(t, &buf, testWriterEntry{
		Event: "event",
		KV:    MergedKV(kv, KV{"timing": timing}),
	})

	sink.Event("event", MergedKV(kv, KV{"gauge": gauge}))
	checkEntry(t, &buf, testWriterEntry{
		Event: "event",
		KV:    MergedKV(kv, KV{"gauge": gauge}),
	})

	sink.Event("event", MergedKV(kv, KV{"status": Success}))
	checkEntry(t, &buf, testWriterEntry{
		Event: "event",
		KV:    MergedKV(kv, KV{"status": Success}),
	})
}

func checkEntry(t *testing.T, buf *bytes.Buffer, entry testWriterEntry) {
	line := buf.String()

	timestampStr := strings.Trim(strings.Split(line, " ")[0], "[]")
	timestamp, err := time.Parse(time.RFC3339Nano, timestampStr)
	require.Nil(t, err, "timestamp should be RFC3339Nano")
	require.WithinDuration(t, time.Now(), timestamp, time.Millisecond, "timestamp should be time.Now")

	if len(entry.Event) > 0 {
		require.Contains(t, line, "event:"+entry.Event)
	}

	require.Contains(t, line, FormattedKV(entry.KV))

	buf.Reset()
}
