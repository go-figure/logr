package logr_test

import (
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-figure/logr"
	"gitlab.com/go-figure/logr/mock"
)

func TestJob(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	kv := logr.KV{
		"int":    1,
		"string": "val",
		"bool":   true,
	}

	err := errors.New("testing error")
	timing := 42 * time.Second
	gauge := 0.42
	status := logr.Success

	sink := mock.NewMockSink(ctrl)
	sink.EXPECT().Event("event", kv)
	sink.EXPECT().Event("error", logr.MergedKV(kv, logr.KV{"error": err}))
	sink.EXPECT().Event("timing", logr.MergedKV(kv, logr.KV{"timing": timing}))
	sink.EXPECT().Event("gauge", logr.MergedKV(kv, logr.KV{"gauge": gauge}))
	sink.EXPECT().Event("job_end", jobCompleteMatcher{status: status})

	job := logr.NewJob(sink, "testing", nil)
	job.KV = logr.KV{
		"int": 1,
	}
	job.KeyValue("string", "val")

	kv = logr.KV{
		"bool": true,
	}

	job.Event("event", kv)
	job.Error("error", err, kv)
	job.Timing("timing", timing, kv)
	job.Gauge("gauge", gauge, kv)
	job.Complete(status, kv)

	job = logr.NewJob(nil, "testing", nil)
	require.Equal(t, logr.Discard, job.Sink, "providing nil as a sink to NewJob should set the Sink to Discard")
}

type jobCompleteMatcher struct {
	status logr.CompletionStatus
}

func (m jobCompleteMatcher) Matches(x interface{}) bool {
	kv, ok := x.(logr.KV)
	if !ok {
		return false
	}

	status, ok := kv["status"]
	if !ok {
		return false
	}

	if status != string(m.status) {
		return false
	}

	timing, ok := kv["timing"]
	if !ok {
		return false
	}

	_, ok = timing.(time.Duration)
	if !ok {
		return false
	}

	return true
}

func (m jobCompleteMatcher) String() string {
	return fmt.Sprintf("logr.KV[any key:any val..., status: %v, timing: any time.Duration]", string(m.status))
}
