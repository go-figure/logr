module gitlab.com/go-figure/logr

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/mock v1.4.1
	github.com/stretchr/testify v1.4.0
	github.com/zemirco/uid v0.0.0-20160129141151-3763f3c45832
)

go 1.13
