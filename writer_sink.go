package logr

import (
	"fmt"
	"io"
	"sort"
	"strings"
	"time"
)

// check that WriterSink implements the Sink interface
var _ Sink = WriterSink{}

type WriterSink struct {
	w io.Writer
}

func NewWriterSink(w io.Writer) WriterSink {
	return WriterSink{w: w}
}

func (s WriterSink) Event(event string, kv KV) {
	fmt.Fprintf(s.w, "[%s] event:%s %s\n",
		time.Now().Format(time.RFC3339Nano),
		event, FormattedKV(kv),
	)
}

func FormattedKV(kv KV) string {
	keys := make([]string, 0, len(kv))
	for k := range kv {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	formatted := make([]string, 0, len(keys))
	for _, k := range keys {
		formatted = append(formatted, FormatField(k, kv[k]))
	}

	return strings.Join(formatted, " ")
}

func FormatField(key string, val interface{}) string {
	return key + ":" + FormatValue(val)
}

func FormatValue(val interface{}) string {
	switch v := val.(type) {
	case fmt.Stringer:
		return v.String()
	case error:
		return v.Error()
	case string:
		return v
	default:
		return fmt.Sprintf("%#v", v)
	}
}

func DisplayValue(val interface{}) interface{} {
	switch v := val.(type) {
	case fmt.Stringer:
		return v.String()
	case error:
		return v.Error()
	case string:
		return v
	default:
		return val
	}
}
