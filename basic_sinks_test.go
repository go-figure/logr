package logr_test

import (
	"errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"gitlab.com/go-figure/logr"
	"gitlab.com/go-figure/logr/mock"
)

func TestTeeSink(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	kv := logr.KV{
		"key1":   1,
		"key2":   "val",
		"key3":   true,
		"error":  errors.New("testing error"),
		"timing": 42 * time.Second,
		"gauge":  0.42,
		"status": logr.Success,
	}

	var ts logr.TeeSink
	for i := 0; i < 10; i++ {
		sink := mock.NewMockSink(ctrl)
		sink.EXPECT().Event("event", kv)
		ts.Add(sink)
	}

	ts.Event("event", kv)
}

func TestKVSink(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	kv := logr.KV{
		"key1":   1,
		"key2":   "val",
		"key3":   true,
		"error":  errors.New("testing error"),
		"timing": 42 * time.Second,
		"gauge":  0.42,
		"status": logr.Success,
	}

	sink := mock.NewMockSink(ctrl)
	sink.EXPECT().Event("event", kv)
	sink.EXPECT().Event("event", kv)

	kvs := logr.KVSink{
		Sink: sink,
	}
	kvs.KeyValue("key1", 1)
	kvs.KeyValue("key2", "val")

	kv = logr.KV{
		"key3":   true,
		"error":  errors.New("testing error"),
		"timing": 42 * time.Second,
		"gauge":  0.42,
		"status": logr.Success,
	}

	kvs.Event("event", kv)

	tKV := kvs.KV
	tKV["key3"] = true
	tKV["error"] = errors.New("testing error")
	tKV["timing"] = 42 * time.Second
	tKV["gauge"] = 0.42
	tKV["status"] = logr.Success
	kvs.KV = nil
	kvs.Event("event", tKV)
}
