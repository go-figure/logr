package logr

import (
	"encoding/json"
	"io"
	"time"
)

// check that JSONWriterSink implements the Sink interface
var _ Sink = JSONWriterSink{}

type jsonEntry struct {
	Timestamp string `json:"timestamp"`
	Event     string `json:"event"`
	KV        KV     `json:"kv"`
}

type JSONWriterSink struct {
	enc *json.Encoder
}

func NewJSONWriterSink(w io.Writer) JSONWriterSink {
	return JSONWriterSink{
		enc: json.NewEncoder(w),
	}
}

func (s JSONWriterSink) encode(e *jsonEntry) {
	e.Timestamp = time.Now().Format(time.RFC3339Nano)
	s.enc.Encode(e)
}

func (s JSONWriterSink) Event(event string, kv KV) {
	s.encode(&jsonEntry{Event: event, KV: kv.humanized()})
}
