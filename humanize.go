package logr

import (
	"fmt"
	"reflect"
	"strings"
)

func humanize(val interface{}, vis visited) interface{} {
	if val == nil {
		return "<nil>"
	}

	// Check for simple types and types that implement fmt.Stringer and error
	switch v := val.(type) {
	case string:
		return v
	case error:
		return v.Error()
	case fmt.Stringer:
		return v.String()
	}

	v := reflect.ValueOf(val)
	switch v.Kind() {
	case reflect.String:
		return v.String()
	case reflect.Bool:
		return v.Bool()
	case reflect.Int:
		return int(v.Int())
	case reflect.Int8:
		return int8(v.Int())
	case reflect.Int16:
		return int16(v.Int())
	case reflect.Int32:
		return int32(v.Int())
	case reflect.Int64:
		return v.Int()
	case reflect.Uint:
		return uint(v.Uint())
	case reflect.Uint8:
		return uint8(v.Uint())
	case reflect.Uint16:
		return uint16(v.Uint())
	case reflect.Uint32:
		return uint32(v.Uint())
	case reflect.Uint64:
		return v.Uint()
	case reflect.Float32:
		return float32(v.Float())
	case reflect.Float64:
		return float64(v.Float())
	case reflect.Complex64, reflect.Complex128:
		return humanizeString(v.Complex())
	case reflect.Chan:
		if v.IsNil() {
			return "<nil>"
		}
		return "<chan>"
	case reflect.Func:
		if v.IsNil() {
			return "<nil>"
		}
		return "<func>"
	case reflect.Interface:
		return "<interface>"
	case reflect.UnsafePointer:
		return "<unsafe ptr>"
	case reflect.Uintptr:
		return "<uintptr>"
	case reflect.Ptr:
		if v.IsNil() {
			return "<nil>"
		}

		addr := v.Pointer()
		if ok := vis.visited(addr); ok {
			return "<cycle>"
		}
		vis = append(vis, addr)

		return humanize(v.Elem().Interface(), vis)
	case reflect.Map:
		addr := v.Pointer()
		if ok := vis.visited(addr); ok {
			return "<cycle>"
		}
		vis = append(vis, addr)

		return humanizeMap(v, vis)
	case reflect.Slice:
		addr := v.Pointer()
		if ok := vis.visited(addr); ok {
			return "<cycle>"
		}
		vis = append(vis, addr)

		return humanizeSlice(v, vis)

	case reflect.Array:
		return humanizeSlice(v, vis)

	case reflect.Struct:
		return humanizeStruct(v, vis)
	}

	return val
}

func humanizeString(val interface{}) string {
	switch v := val.(type) {
	case string:
		return escapeString(v)

	case []interface{}:
		var d string = "["
		for i, elem := range v {
			d += humanizeString(elem)
			if i != len(v)-1 {
				d += ", "
			}
		}
		d += "]"
		return d

	case map[string]interface{}:
		var (
			d string = "{ "
			i int
		)
		for k, val := range v {
			d += fmt.Sprintf("%s: %s", k, humanizeString(val))

			if i != len(v)-1 {
				d += ", "
			}

			i++
		}
		d += " }"
		return d
	}

	return fmt.Sprintf("%v", val)
}

func humanizeMap(v reflect.Value, vis visited) map[string]interface{} {
	m := make(map[string]interface{})

	iter := v.MapRange()
	for iter.Next() {
		k := iter.Key()
		v := iter.Value()
		m[humanizeString(k)] = humanize(v.Interface(), vis)
	}

	return m
}

func humanizeSlice(v reflect.Value, vis visited) []interface{} {
	slen := v.Len()
	slice := make([]interface{}, slen)
	for i := 0; i < slen; i++ {
		val := v.Index(i).Interface()

		slice[i] = humanize(val, vis)
	}

	return slice
}

func humanizeStruct(v reflect.Value, vis visited) map[string]interface{} {
	s := make(map[string]interface{})
	flen := v.NumField()

	for i := 0; i < flen; i++ {
		name := v.Type().Field(i).Name
		val := v.Field(i)

		s[name] = humanize(val.Interface(), vis)
	}

	return s
}

func escapeString(s string) string {
	if strings.ContainsAny(s, " '\"`\t\n[]{}!@#$%^&*()-=") {
		return fmt.Sprintf("%q", s)
	}

	return s
}

type visited []uintptr

func (v visited) visited(addr uintptr) bool {
	for _, a := range v {
		// Already visited
		if a == addr {
			return true
		}
	}

	return false
}
