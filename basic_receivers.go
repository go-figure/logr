package logr

import "time"

// check that KVReceiver implementes the Receiver interface
var _ Receiver = (*KVReceiver)(nil)

type KVReceiver struct {
	Receiver Receiver
	KV       KV
}

func (rcv *KVReceiver) KeyValue(key string, val interface{}) {
	if rcv.KV == nil {
		rcv.KV = make(KV)
	}

	rcv.KV[key] = val
}

func (rcv KVReceiver) Event(event string, kv KV) {
	rcv.Receiver.Event(event, MergedKV(rcv.KV, kv))
}

func (rcv KVReceiver) Error(event string, err error, kv KV) {
	rcv.Receiver.Error(event, err, MergedKV(rcv.KV, kv))
}

func (rcv KVReceiver) Timing(event string, timing time.Duration, kv KV) {
	rcv.Receiver.Timing(event, timing, MergedKV(rcv.KV, kv))
}

func (rcv KVReceiver) Gauge(event string, gauge float64, kv KV) {
	rcv.Receiver.Gauge(event, gauge, MergedKV(rcv.KV, kv))
}
