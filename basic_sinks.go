package logr

// check that all types implement the Sink interface
var _ Sink = DiscardSink{}
var _ Sink = TeeSink{}
var _ Sink = (*KVSink)(nil)

var Discard = DiscardSink{}

type DiscardSink struct{}

func (_ DiscardSink) Event(event string, kv KV) {}

type TeeSink []Sink

func (ts *TeeSink) Add(sink Sink) {
	*ts = append(*ts, sink)
}

func (ts TeeSink) Event(event string, kv KV) {
	for _, sink := range ts {
		sink.Event(event, kv)
	}
}

type KVSink struct {
	Sink Sink
	KV   KV
}

func (s *KVSink) KeyValue(key string, val interface{}) {
	if s.KV == nil {
		s.KV = make(KV)
	}

	s.KV[key] = val
}

func MergedKV(kv1, kv2 KV) KV {
	if kv1 == nil {
		return kv2
	}
	if kv2 == nil {
		return kv1
	}

	merged := make(KV)

	for k, v := range kv1 {
		merged[k] = v
	}

	for k, v := range kv2 {
		merged[k] = v
	}

	return merged
}

func (s KVSink) Event(event string, kv KV) {
	s.Sink.Event(event, MergedKV(s.KV, kv))
}
